CREATE DATABASE  IF NOT EXISTS `testdb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `testdb`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: testdb
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author_details`
--

DROP TABLE IF EXISTS `author_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author_details` (
  `author_id` int NOT NULL AUTO_INCREMENT,
  `author_name` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`author_id`),
  UNIQUE KEY `author_name_UNIQUE` (`author_name`),
  UNIQUE KEY `description_UNIQUE` (`description`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_details`
--

LOCK TABLES `author_details` WRITE;
/*!40000 ALTER TABLE `author_details` DISABLE KEYS */;
INSERT INTO `author_details` VALUES (1,'R P N Singh','1964-04-04','The book was written by R P N Singh which throws light on the subject related to rise of regional parties and Centre -States relation.','2020-03-09 09:03:10','Ram','2020-03-09 09:03:10','Ram',1),(2,'Vasdev Mohi','1944-05-09','“Chequebook” also highlights the life of domestic maid-servant who has to suffer on account of ill- treatment at the hands of her husband and poverty.','2020-05-19 06:18:40','Anil','2020-05-19 06:18:40','Anil',1),(3,'Malcom Gladwell','1963-03-09','Malcolm Gladwell takes us on an intellectual journey through the world of \"outliers\"--the best and the brightest, the most famous and the most successful.','2020-07-08 08:03:10','Kamlesh','2020-07-08 08:03:10','Kamlesh',1),(4,'Brian Tracy','1944-01-05','eating a frog is a metaphor for tackling your most challenging task—but also the one that can have the greatest positive impact on your life.','2019-03-09 10:03:10','Rani','2019-03-09 10:03:10','Rani',1),(5,'Robin Sharma','1964-06-06','The Monk Who Sold His Ferrari is a self-help book by Robin Sharma, a writer and motivational speaker.','2020-04-08 05:09:20','Ram','2020-04-08 05:09:20','Ram',1);
/*!40000 ALTER TABLE `author_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_details`
--

DROP TABLE IF EXISTS `book_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_details` (
  `book_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `author` varchar(45) NOT NULL,
  `publisher` varchar(45) NOT NULL,
  `genre` varchar(45) NOT NULL,
  `release_year` date NOT NULL,
  `rating` int NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `status` int NOT NULL,
  `recommend_user` varchar(45) NOT NULL,
  `wishlist` varchar(45) NOT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `book_id_UNIQUE` (`book_id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_details`
--

LOCK TABLES `book_details` WRITE;
/*!40000 ALTER TABLE `book_details` DISABLE KEYS */;
INSERT INTO `book_details` VALUES (1,'Politics of Opportunism','R P N Singh','Vitasta Publishing Pvt. Ltd','political','2019-06-07',3,'2020-03-09 09:03:10','Ram','2020-03-09 09:03:10','Ram',1,'Anil','Added'),(2,'Cheque book','Vasdev Mohi','White Falcon Self Publishing Platform','Indian Writing','2018-09-10',4,'2020-05-19 06:18:40','Anil','2020-05-19 06:18:40','Anil',1,'Kamlesh',''),(3,'Outliers','Malcom Gladwell','Allen Lane','Self-help book','2011-08-12',5,'2020-07-08 08:03:10','Kamlesh','2020-07-08 08:03:10','Kamlesh',1,'Rani',''),(4,'Eat that Frog','Brian Tracy','TMH','Self-help book','2007-03-06',5,'2019-03-09 10:03:10','Rani','2019-03-09 10:03:10','Rani',1,'Ram','');
/*!40000 ALTER TABLE `book_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publishers_details`
--

DROP TABLE IF EXISTS `publishers_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publishers_details` (
  `publishers_id` int NOT NULL AUTO_INCREMENT,
  `established_on` date NOT NULL,
  `address` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `status` int NOT NULL,
  `publisher_name` varchar(45) NOT NULL,
  PRIMARY KEY (`publishers_id`),
  UNIQUE KEY `publisher_name_UNIQUE` (`publisher_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publishers_details`
--

LOCK TABLES `publishers_details` WRITE;
/*!40000 ALTER TABLE `publishers_details` DISABLE KEYS */;
INSERT INTO `publishers_details` VALUES (1,'2014-08-09','India','White Falcon Publishing provides services to authors to self publish books in India for global distribution.','2020-05-19 06:18:40','Anil','2020-05-19 06:18:40','Anil',1,'White Falcon Self Publishing Platform'),(2,'2004-07-07','New delhi','Vitasta is an independent, New Delhi based publishing house, founded in 2004.','2020-03-09 09:03:10','Ram','2020-03-09 09:03:10','Ram',1,'Vitasta Publishing Pvt. Ltd');
/*!40000 ALTER TABLE `publishers_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_reg`
--

DROP TABLE IF EXISTS `user_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_reg` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `user_type` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phone` int NOT NULL,
  `created_on` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(45) NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `first_name_UNIQUE` (`first_name`),
  UNIQUE KEY `last_name_UNIQUE` (`last_name`),
  UNIQUE KEY `address_UNIQUE` (`address`),
  UNIQUE KEY `phone_UNIQUE` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_reg`
--

LOCK TABLES `user_reg` WRITE;
/*!40000 ALTER TABLE `user_reg` DISABLE KEYS */;
INSERT INTO `user_reg` VALUES (1,'Ram','Kumar','M','1991-10-05','Normal','Sector-A',44445,'2020-03-09 09:03:10','Ram','2020-03-09 09:03:10','Ram',1),(2,'Anil','Kapoor','M','1994-01-04','Premium','Sector-B',47445,'2020-05-19 06:18:40','Anil','2020-05-01 06:18:40','Anil',1),(3,'Kamlesh','Kothari','M','1998-10-02','Normal','Sector-C',43445,'2020-07-08 08:03:10','Kamlesh','2020-07-08 08:03:10','Kamlesh',1),(4,'Rani','Singh','F','1995-06-04','Premium','Sector-D',48445,'2019-03-09 10:03:10','Rani','2019-03-09 10:03:10','Rani',1);
/*!40000 ALTER TABLE `user_reg` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 14:00:50
