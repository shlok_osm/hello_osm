use testdb;

INSERT INTO `testdb`.`author_details` (`author_id`, `author_name`, `dob`, `description`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`) VALUES ('5', 'Robin Sharma', '1964-06-06', 'The Monk Who Sold His Ferrari is a self-help book by Robin Sharma, a writer and motivational speaker.', '2020-04-08 05:09:20', 'Ram', '2020-04-08 05:09:20', 'Ram', '1');

INSERT INTO `testdb`.`publishers_details` (`publishers_id`, `established_on`, `address`, `description`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`, `publisher_name`) VALUES ('2', '2004-07-07', 'New delhi', 'Vitasta is an independent, New Delhi based publishing house, founded in 2004.', '2020-03-09 09:03:10', 'Ram', '2020-03-09 09:03:10', 'Ram', '1', 'Vitasta Publishing Pvt. Ltd');
INSERT INTO `testdb`.`publishers_details` (`publishers_id`, `established_on`, `address`, `description`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`, `publisher_name`) VALUES ('2', '2014-08-09', 'India', 'White Falcon Publishing provides services to authors to self publish books in India for global distribution.', '2020-05-19 06:18:40', 'Anil', '2020-05-19 06:18:40', 'Anil', '1', 'White Falcon Self Publishing Platform');

SELECT title FROM book_details WHERE created_by = "Ram";

SELECT title FROM book_details WHERE created_on = "2020-03-09 09:03:10";

SELECT recommend_user FROM book_details WHERE created_by = "Ram";

UPDATE `testdb`.`book_details` SET `wishlist` = 'Added' WHERE (`book_id` = '1');

SELECT * FROM user_reg WHERE user_type ="Normal";

SELECT * FROM user_reg WHERE gender ="F";

SELECT genre FROM book_details;

SELECT * FROM book_details WHERE rating > 4;

SELECT * FROM book_details WHERE rating = 5;

SELECT * FROM book_details WHERE rating = 3;

SELECT * FROM author_details WHERE author_name LIKE'AR%';

SELECT * FROM publishers_details WHERE established_on < 2012-12-12;
