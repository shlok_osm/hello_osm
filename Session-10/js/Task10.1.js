function charFreq(s) {
    var freq = {};
    for(var i = 0; i < s.length ; i++) {
        var character = s.charAt(i); 
        if (freq[character]) {
            freq[character]++;
         } else {
            freq[character] = 1;
         }
    }
    return freq;
};

function main() {
    var s = prompt('enter string:');
    charFreq(s);
}
main();