function converttoarray(arr) {
    var a = arr.split(",");
    var arr2 = [];
    for(var i = 0; i < a.length; i++) {
        arr2[i] = Number(a[i]);
    }
    return (arr2);
}

function pairnearzero(arr2, n) {
    let inv_count = 0; 
  let l, r, min_sum, sum, min_l, min_r; 
  
  /* Array should have at least two elements*/
  if(n < 2) 
  { 
    console.log("Invalid Input"); 
    return; 
  } 
  
  /* Initialization of values */
  min_l = 0; 
  min_r = 1; 
  min_sum = arr2[0] + arr2[1]; 
  
  for(l = 0; l < n - 1; l++) 
  { 
    for(r = l+1; r < n; r++) 
    { 
      sum = arr2[l] + arr2[r]; 
      if(Math.abs(min_sum) > Math.abs(sum)) 
      { 
        min_sum = sum; 
        min_l = l; 
        min_r = r; 
      } 
    } 
  } 
  
  console.log(" The two elements whose sum is closest to zero are %d and %d", 
          arr2[min_l], arr2[min_r]); 
}




function main() {
    var arr = prompt('enter array elements:');
    var a = converttoarray(arr);
    var n = a.length;
    var b = pairnearzero(a, n);
    console.log(b);
}
main();