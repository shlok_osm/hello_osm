/*package whatever //do not write package name here 

	Inner class types:
	1 Member class-which does not have static[contains instance fields]
	2 Static class-which is static...eg.:
			static class Inner {
				public void display()
			{
            System.out.println("in display");
			}
		}
	3 Anonymous class*/

import java.util.*;
import java.lang.*;
import java.io.*;2
//when you compile there would be 3 class files
class Outer
{
    int a;
    public void show() 
    {
        
    }
    
    class Inner//Outer$Inner.class
    {
        public void display()
        {
            System.out.println("in display");
        }
    }
}   
 
class InnerDemo {
	public static void main (String[] args) {
		//code
	    Outer obj = new Outer();
	    obj.show();
	    
	    Outer.Inner obj1 = obj.new Inner();
	    obj1.display();
		
		//If line 9,10,15 are static, then...
		//Outer.Inner obj1 = new Outer.Inner();
	}
}