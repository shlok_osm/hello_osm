import java.util.*;
public class BinarySearch {
	public static String findNumber(List<Integer> arr, int k) {
	     int l = 0; 
	     int h = arr.size() - 1;
	     boolean found = false;
	     arr.sort(Comparator.naturalOrder());
	           while(l <= h){
	           	   int mid = (l + h)/2;
	        	   if(k == arr.get(mid)){ // arr[mid]
	        			found = true;
	        			break;
	        	   }
	        	   else if(k < arr.get(mid)){
	        	   	 h = mid - 1;
	        	   }
	        	   else{
	        	   	l = mid + 1;
	        	   }
	           }
		   if(found == true){
		   		return "YES";
		   }
	    return "NO";
	   }
	public static void main(String[] args) {
		List<Integer> l = Arrays.asList(2,7,1,9,5,3,4);
		System.out.println(findNumber(l, 9));

	}

}
