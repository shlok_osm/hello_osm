import java.io.*;
import java.util.*;
/*
    arr ->  10 20  30 40 50
    Idx     0  1   2  3  4
    
    HLT
    
    Expectation[E]    |   Faith[F]    |   E + F
      dar(arr,0)          dar(arr,1)      dar(arr,0) = dar(arr,0)
          50                  50                        10
          40                  40
          30                  30
          20                  20
          10
          
    LLT
    
    dar    4k  | 5 [wiped out]
    dar    4k  | 4 . .                   1st '.' = call
    dar    4k  | 3 . .                   2nd '.' = print
    dar    4k  | 2 . .
    dar    4k  | 1 . .
    dar    4k  | 0 . .
    main - 4k  | 
           arr  idx
    
*/
public class ReversearrayRecursion{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        
        int arr[] = new int[n];
        for(int i = 0; i < n; i++)
        {
            arr[i] = s.nextInt();
        }
        
        displayArrReverse(arr,0);
    }

    public static void displayArrReverse(int[] arr, int idx) {
        if(idx == arr.length)
        {
            return;
        }
        
        displayArrReverse(arr, idx+1);
        System.out.println(arr[idx]);   
        
    }

}