/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;


public class Anagrams3a {
	public static void main (String[] args) {
	    
	    String a = "act";
	    String b = "cat";
	    
	    boolean isAnagram = true;
	   
	    int al[] = new int[256];
	    int bl[] = new int[256];
	    
	    for(char c: a.toCharArray())
	    {
	        int index = (int) c;// taking out index of every character in 'a'
	        al[index]++;//On that index--->0+1=1 and so on
	    }
	    
	    for(char c: b.toCharArray())
	    {
	        int index = (int) c;
	        bl[index]++;
	    }
	    
	    for(int i = 0; i< 256; i++)
	    {
	        if(al[i] != bl[i])
	        {
	            isAnagram = false;
	            break;
	        }
	    }
	    
	    if(isAnagram) 
	    {
	        System.out.println("anagram");
	    }
	    else
	    {
	        System.out.println("not anagram");
	    }
	}
}

