import java.util.*;
  
  public class Decimaltoanybase{
  
  public static void main(String[] args) {
      Scanner scn = new Scanner(System.in);
      int n = scn.nextInt();
      int b = scn.nextInt();
      int dn = getValueInBase(n, b);
      System.out.println(dn);
   }
  
   public static int getValueInBase(int n, int b){
       // My way...
       int c = 0;
       int count = 0;
       while(n!=0)
       {
           int r = (n % b) * (int) Math.pow(10,count);
           n /= b;
           c += r;
           count++;
       }
       return c;
   }
  }
  
  /*
   public static int getValueInBase(int n, int b){
       // Prescribed approach..
       int p = 1;
	   int rv = 0;
       while(n!=0)
       {
           int r = (n % b);
           n /= b;
           
		   rv += r * p;
		   p *= 10;
       }
       return rv;
   }
   */