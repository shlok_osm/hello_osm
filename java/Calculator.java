/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;



public class Calculator {
	public static void main (String[] args) throws Exception{
	    
	    Scanner s = new Scanner(System.in);
	    
	    System.out.println("Enter first number ");
	    int a = s.nextInt();
	    
	    System.out.println("Enter second number ");
	    int b = s.nextInt();
	    
	    System.out.println("Enter the operation ");
	    s.nextLine();
	    char op = s.nextLine().charAt(0);
	    
	    int result = 0;
	    
	    switch(op) {
	        case '+':
	            result = a + b;
	            break;
	        case '-':
	            result = a - b;
	            break;
	        case '*':
	            result = a * b;
	            break;
	        case '/':
	            result = a / b;
	            break;  
	        default:
	            System.out.println("Invalid operation");
	    }
	    
	    System.out.println("result: " + result);
	}
}