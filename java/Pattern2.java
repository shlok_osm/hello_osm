/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;

//To print..
/*
        ----1--
        ---1-2--
        --1-2-3--
        -1-2-3-4--
        1-2-3-4-5--
*/

public class Pattern2 {
	public static void main (String[] args) {
	    
	   Scanner s = new Scanner(System.in);
	   int n = s.nextInt();
	    
	    for(int i = 1; i <= n; i++)
	    {
	        String s1 = "";
	        for(int j = 1; j <= n-i; j++){
	            s1 = s1 + "  ";
	            //System.out.print( "*"  + " ");
	        }
	        for(int j = 1; j <= i; j++)
	        {
	            s1 = s1 + j + "  ";
	        }
	        System.out.println(s1);
	    }
	   
	    
	}
}