/*package whatever //do not write package name here 

Given a stream of numbers, print average or mean of the stream at every point.

Input
2--->Testcase
5--->Array size
10 20 30 40 50--->Array elements
2
12 2

Output
10 15 20 25 30--->10/1=10; 10+20/2=15; 10+20+30/3=20; 10+20+30+40/4=25; 10+20+30+40+50/5=30
12 7
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class Average {
	public static void main (String[] args) {
		
// 		public static int avgNumber(int ... args){
            
//             int sum = 0;
//             for(int x: args){
//                 sum += x;
//         }
//         return sum;
//     }
		Scanner s = new Scanner(System.in);
		int t = s.nextInt();
		while(t-- > 0) {
		    int n = s.nextInt();
		    int c[] = new int[n];
		    for(int i = 0; i < n; i++) {// c:1 3 2     
		        c[i] = s.nextInt();
		    }
		    int a[] = new int[n];
		    a[0] = c[0];
		    for(int i = 1; i < n; i++) {
		        a[i] = a[i-1] + c[i]; // a:1 4 6 
		    }
		    for(int i = 0;i<n;i++ )
		        System.out.print(a[i]/(i+1) + " ");
		    System.out.println();
		}
	}
}