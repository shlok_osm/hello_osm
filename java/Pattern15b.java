import java.util.*;
/*
    *----    1,1   sp = i - 1
    -*---    2,2
    --*--    3,3
    ---*-    4,4
    ----*    5,5
    i = j at diagonals of a square
*/
public class Pattern15b {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        
        for(int i = 1; i <= n; i++)
        {
            StringBuilder s = new StringBuilder("");
            for(int j = 1; j <= n; j++)
            {
                if(i==j)
                {
                   s.append("*\t");
                }
                else
                {
                   s.append("\t");    
                }
            }
            
            System.out.println(s);
        }
        // write ur code here

    }
}