/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;

//Multiple Inheritance problem in interface --> default and how to fix it

interface Demo
{
    void abc();
    default void show()
    {
        System.out.println("in Demo show");
    }
}

interface MyDemo
{
    default void show()
    {
        System.out.println("in MyDemo show");
    }
}
class DemoImp implements Demo, MyDemo
{
    public void abc()
    {
        System.out.println("in abc");
    }
    //solution
    @Override
    public void show()
    {
        Demo.super.show();//solution
		//MyDemo.super.show();
    }
}

public class InterfacesDemo {
	public static void main (String[] args) {
	   Demo obj = new DemoImp();
	   obj.abc();
	   obj.show();
	}
}