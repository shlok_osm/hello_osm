/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

abstract class Human
{
    public abstract void eat();
    
    public void walk()
    {
        
    }
}
class Man extends Human //Concrete
{
    public void eat()
    {
        
    }
}
public class AbstractDemo {
    
	public static void main (String[] args) {
		//code
	   Human obj = new Man();
	}
}