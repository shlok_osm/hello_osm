import java.io.*;
import java.util.*;

//2-D Array

public class SortingTwoDarray{

public static void main(String[] args) throws Exception {
    // write your code here
    int a[][] = { {2,9,4,5},
                  {11,14,13,12},
                  {18,17,16,20},
                  {25, 33,12, 9}
        };
        for(int x[] : a){
            Arrays.sort(x);
        }
        for(int s[] : a){
            for(int c : s){
                System.out.print(c + " ");
            }
            System.out.println();
        }
 }

}