import java.io.*;
import java.util.*;

// Print Permutations of string - abc[say]
// abc,acb,bac,bca,cab,cba

// https://pasteboard.co/JBPpigP.png

public class PrintPermutations {

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        String ques = s.next();
        printPermutations(ques,"");
    }

    public static void printPermutations(String ques, String ans) {
        if(ques.length() == 0)
        {
            System.out.println(ans);
            return;
        }
        
        for(int i = 0; i < ques.length(); i++)
        {
            char ch = ques.charAt(i);
            String rol = ques.substring(0,i);  //left of ith character
            String ror = ques.substring(i+1); //right of ith character
            String roq = rol + ror;
            printPermutations(roq, ans + ch);
        }
    }

}