/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;



//Only runtime subclasses are unchecked exception


public class ExceptionDemo {
	public static void main (String[] args) {
	    try
	    {
	        int a[] = new int[6];
	        a[6] = 8;
	        int i = 7;
	        int j = 0;
	        int k = i/j;
	        System.out.println("output is" + k);
	    }
	    catch(ArithmeticException | ArrayIndexOutOfBoundsException e ) //cannot write multiple catch if you are using Java 1.6 or below.
	    {
	        System.out.println("Error");
	    }
	    finally
	    {
	        System.out.println("Bye");
	    }
	}
}

/*
Could have also been...
catch(ArithmeticException  e )
	    {
	        System.out.println("Cannot divide by Zero");
	    }
	    catch(ArrayIndexOutOfBoundsException e)
	    {
	       System.out.println("Stay in your limit");
	    }
		catch(Exception e)//Do not put this as the 1st catch because then what will other catch blocks do?--->For unknown exception for which there is no catch block
	    {
	        System.out.println("Something wrong...");
	    }
		O/P = Stay in your limit
*/