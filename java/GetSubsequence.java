import java.io.*;
import java.util.*;

// https://pasteboard.co/Jy9VP04.png

// https://pasteboard.co/Jy9WOBD.png

public class GetSubsequence{

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        String str = s.next();             //abc[suppose]
        ArrayList<String> res = gss(str);
        System.out.println(res);
    }

    public static ArrayList<String> gss(String str) {
        //Base-case:
        //There will be 1 subsequence as 2^0 = 1, but it'll be blank
        if(str.length() == 0)
        {
            ArrayList<String> bres = new ArrayList<>();
            bres.add("");
            return bres;
        }
        
        char ch = str.charAt(0);					//a
        String ros = str.substring(1);				//bc
        ArrayList<String> rres = gss(ros);			//[--,-c,b-,bc]
        
        ArrayList<String> mres = new ArrayList<>();
        for(String rstr : rres)
        {
            mres.add("" + rstr);
        }
        for(String rstr : rres)
        {
            mres.add(ch + rstr);
        }
        return mres;
    }

}