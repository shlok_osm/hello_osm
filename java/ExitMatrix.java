import java.io.*;
import java.util.*;
/*            dir  i  j    
    0 0 1 0    0   0  0
    1 0 0 1    0   0  1
    0 0 0 1    0   0  2
    1 0 1 0    1   1  2
               1   2  2
               1   3  2
               2   3  1
               2   3  0
               3   2  0
               3   1  0
               0   1  1
               0   1  2
               0   1  3
               1   2  3
               2   2  2
               2   2  1
               2   2  0
               2   2  -1-->out
*/             
public class  ExitMatrix{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int m = s.nextInt();
    
        int a[][] = new int[n][m];
    
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < m; j++)
            {
                a[i][j] = s.nextInt();
            }
        }
        
        int dir = 0;//0-e, 1-s, 2-w, 3-n
        int i = 0;
        int j = 0;
        
        while(true)
        {
            dir = (dir + a[i][j]) % 4;
            
            if(dir == 0) //east
            {
                j++;
            }
            else if(dir == 1) //south
            {
                i++;
            }
            else if(dir == 2) //west
            {
                j--;
            }
            else if(dir == 3) //north
            {
                i--;
            }
            
            //To get outside of array...
            if(i < 0)
            {
                i++;//we need exit point and that should'nt be -ve
                break;
            }
            else if(j < 0)
            {
                j++;
                break;
            }
            else if(i == a.length)
            {
                i--;//need exit point and that should'nt be out of range
                break;
            }
            else if(j == a[0].length)
            {
                j--;
                break;
            }
        }
        System.out.println(i);
        System.out.println(j);
    }

}