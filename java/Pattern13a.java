import java.util.*;
/*
  --*
  -***
  *****
  -***
  --*
*/
public class Pattern13a {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        
        int sp = n/2;
        int st = 1;
        int c = n/2 + 1;
        int c2 = c + 1;
        for(int i = 1; i <= c; i++)
        {
            for(int j = 1; j <= sp; j++)
            {
                System.out.print("\t");
            }
            
            for(int j = 1; j <= st; j++)
            {
                System.out.print("*\t");
            }
            sp--;
            st += 2;
            System.out.println();
        }
        int osp = 0;
        st -= 4;
        for(int i = c2; i <= n; i++)
        {
            for(int j = 1; j <= osp; j++)
            {
                System.out.print("\t");
            }
            
            for(int j = 1; j <= st; j++ )
            {
                System.out.print("\t*");
            }
            osp++;
            st -= 2;
            System.out.println();
        }
        // write ur code here
        
    }
}