import java.io.*;
import java.util.*;

public class Recursion4a{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        System.out.println(factorial(n));
    }

    public static int factorial(int n){
        int val = 1;
        if(n > 0)
        {
           val = n * val;
           val = factorial(n-1) * val;
        }
        return val;
    }

}