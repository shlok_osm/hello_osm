/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
// Encapsulation -> Binding data with methods --> to make data safe 

//Eclipse --> Right click --> Source --> Generate Getters & Setters
class Student {
    private int rollno;
    private String name;
    
    //Getters & Setters
    
    public void setRollno(int r)
    {
        rollno = r;
    }
    public int getRollno()
    {
        return rollno;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}

public class EncapsulationDemo {
	public static void main (String[] args) {
		//code
	   Student s1 = new Student();
	   s1.setRollno(2);
	   s1.setName("Shlok");
	   //Avoid --> s1.rollno;
	   //Avoid --> s1.name = "Shlok";
	   
	   
	   System.out.println(s1.getRollno());
	   System.out.println(s1.getName());
	}
}