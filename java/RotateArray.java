import java.io.*;
import java.util.*;

public class RotateArray{
  public static void display(int[] a){
    StringBuilder sb = new StringBuilder();

    for(int val: a){
      sb.append(val + " ");
    }
    System.out.println(sb);
  }
  
  public static void reverse(int[] a, int i, int j)
  {
      int li = i; //left index
      int ri = j; //right index
      
      while(li < ri)
      {
          int temp = a[li];
          a[li] = a[ri];
          a[ri] = temp;
          
          li++;
          ri--;
      }
  }

  public static void rotate(int[] a, int k){// 1 2 3 4 | 5 6 7  ;k = 3
    // write your code here
    int n = a.length;
    k = k % n;
    if(k < 0)
    {
        k = k + n;
    }
    
    //Part-1
    reverse(a,0,n-k-1); //1 2 3 4 --> 4 3 2 1 | 5 6 7
    //Part-2
    reverse(a,n-k,n-1); //4 3 2 1 | 7 6 5
    //all
    reverse(a,0,n-1); //5 6 7 1 2 3 4
  }

public static void main(String[] args) throws Exception {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    int n = Integer.parseInt(br.readLine());
    int[] a = new int[n];
    for(int i = 0; i < n; i++){
       a[i] = Integer.parseInt(br.readLine());
    }
    int k = Integer.parseInt(br.readLine());

    rotate(a, k);
    display(a);
 }

}