import java.util.*;
/*
    ----* 1,5   i + j = n + 1
    ---*- 2,4
    --*-- 3,3
    -*--- 4,2
    *---- 5,1
*/
public class Pattern16 {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        
        for(int i = 1; i <= n; i++)
        {
            StringBuilder s = new StringBuilder("");
            for(int j = 1; j <= n; j++)
            {
                if(i + j == n + 1)
                {
                    s.append("*\t");
                }
                else
                {
                    s.append("\t");
                }
                
            }
            System.out.println(s);
        }
        // write ur code here

    }
}