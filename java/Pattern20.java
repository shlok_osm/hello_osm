import java.util.*;
/*
    *-----* //2 stars in this row
    **---**
    ***-***
    *******
    ....,,,
    -n--
        -n-1-
        
    sp = n + n-1 = 2n -1 
    2n-1-1-1[since 2 stars in i=1] = 2n-3
*/
public class Pattern20{

public static void main(String[] args) {
    Scanner scn = new Scanner(System.in);
    int n = scn.nextInt();
    
    int sp = 2 * n - 3;
    int st = 1;
    
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= st; j++)
        {
            System.out.print("*\t");
        }
        
        for(int j = 1; j <= sp; j++)
        {
            System.out.print("\t");
        }
        
        if(i == n)
        {
            st--;
        }
        
        for(int j = 1; j <= st; j++)
        {
            System.out.print("*\t");
        }
        
        sp -= 2;
        st++;
        System.out.println();
    }
     // write ur code here

 }
}