/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;



public class Anagrams2 {
	public static void main (String[] args) {
	  
	    String s1 = "silent";
	    String s2 = "listen";
	    
	    char[] ch = s1.toCharArray();
	    char[] ch2 = s2.toCharArray();
	    Arrays.sort(ch);
	    Arrays.sort(ch2);
	    String sorted1 = new String(ch);// Can also use--->String.valueOf(ch);--->gives string representation of the character array
	    String sorted2 = new String(ch2);// Can also use--->String.valueOf(ch2);--->gives string representation of the character array
	    
	    if(sorted1.equals(sorted2))
	    {
	        System.out.println("anagram");
	    }
	    else
	    {
	        System.out.println("not anagram");
	    }
	}
}

