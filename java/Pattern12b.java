import java.util.*;
/*
    *****
     ****
      ***
       **
        *
*/
public class Pattern12b {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        
        for(int i =1 ; i <= n; i++)
        {
            StringBuilder s = new StringBuilder("");
            for(int j = 1; j <= i-1; j++ )
            {
                s.append("\t"); 
            }
            
            for(int j = 1; j <= n+1-i; j++)
            {
                s.append("*\t");
            }
            System.out.println(s);
        }
        // write ur code here

    }
}