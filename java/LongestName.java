/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
/*
Given a list of names, display the longest name
Input:
1// No. of testcases
5 //No. of names
Geek
Geeks
Geeksfor
GeeksforGeek
GeeksforGeeks

Output:
GeeksforGeeks
*/

class LongestName {
	public static void main (String[] args) {
		//code
		Scanner s = new Scanner(System.in);
		int t = s.nextInt();
		while(t-- >0) {
		    int n = s.nextInt();
		    String[] names = new String[n];
		    s.nextLine();
		    for(int i=0; i < n; i++){
		        names[i] = s.nextLine();
		    }
		    int a[] = new int[n];
		    for(int i=0; i < n; i++)
		         a[i] = names[i].length();
		    Arrays.sort(a);
		    for(int i=0; i < n; i++) {
		        if(names[i].length()==a[n-1]){
		            System.out.println(names[i]);
		        }
		    }
		}
	}
}