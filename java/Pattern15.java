import java.util.*;
/*
    *       sp = i - 1
    -*
    --*
    ---*
    ----*
*/
public class Pattern15 {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        
        for(int i = 1; i <= n; i++)
        {
            StringBuilder s = new StringBuilder("");
            for(int j = 1; j <= i-1; j++)
            {
                s.append("\t");
            }
            s.append("*\t");
            System.out.println(s);
        }
        // write ur code here

    }
}