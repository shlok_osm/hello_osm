/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;

//To print..
/*
        *-
        *-*
        *-*-*       
        *-*-*-*     
        *-*-*     n=4,i=5,*=3  i-2  2n-i= 8-5=3  7-5+1=3[num-i+1]
        *-*       n=4.i=6,*=2  i-n  2n-i= 8-6=2  7-6+1=2
        *-        n=4,i=7,*=1   2n-i = 8-7=1     7-7+1=1
*/

public class Pattern4 {
	public static void main (String[] args) {
	    
	   Scanner s = new Scanner(System.in);
	   int n = s.nextInt();
	    
	    int num = (2*n)-1;
	    for(int i = 1; i <= num; i++)
	    {
	        String s1 = "";
	        
	        if(i<=n)
	        {
	            for(int j = 1; j <= i; j++){
	            s1 = s1 + "* ";
	           
	            }
	        }
	        else
	        {
	            for(int j = 1; j <= 2*n-i; j++)
	            {
	                s1 = s1 + "* ";
	            }
	        }
	        System.out.println(s1);
	       
	    }
	   
	    
	}
}
