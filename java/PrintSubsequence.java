import java.io.*;
import java.util.*;

//  https://pasteboard.co/JAuxSwi.png

// In this type of recursion we go up in Euler and the answer is solved in base
// case

public class PrintSubsequence {

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        String ques = s.nextLine();
        printSS(ques,"");
    }

    public static void printSS(String ques , String ans) {
        if(ques.length() == 0)
        {
            System.out.println(ans);
            return;
        }
        
        char ch = ques.charAt(0);
        String roq = ques.substring(1);
        
        printSS(roq,ans+ch);//Yes
        printSS(roq,ans+"");//No
        
        
    }

}