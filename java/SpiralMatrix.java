import java.io.*;
import java.util.*;
/*                        minc        maxc          
   [minr,minc]  minr-->    [           ]            
                           [           ]            
                           [           ]            
                maxr-->    [           ] <---[maxr,maxc]
*/
public class SpiralMatrix {

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int m = s.nextInt();
    
        int a[][] = new int[n][m];
    
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < m; j++)
            {
                a[i][j] = s.nextInt();
            }
        }
        /*
            lw - minc
            bw - maxr
            rw - maxc
            tw - minr
            
            && cnt < tne --> not every iteration has 4 walls
        */
        int minr = 0;
        int minc = 0;
        int maxr = a.length-1;
        int maxc = a[0].length-1;
        int tne = n*m; //total number of elements
        int cnt = 0; //counter-->till when I've to print
        
        while(cnt < tne)
        {
            //left wall[lw]-->column[minc] remains constant
            for(int  i = minr, j = minc; i <= maxr && cnt < tne; i++)
            {
                System.out.println(a[i][j]);
                cnt++;
            }
            minc++;//handling corner elements
            
            //bottom wall[bw]-->row[maxr] remains constant
            for(int  i = maxr, j = minc; j <= maxc && cnt < tne; j++)
            {
                System.out.println(a[i][j]);
                cnt++;
            }
            maxr--;//handling corner elements
            
            //right wall[rw]-->column[maxc] remains constant
            for(int  i = maxr, j = maxc; i >= minr && cnt < tne; i--)
            {
                System.out.println(a[i][j]);
                cnt++;
            }
            maxc--;//handling corner elements
            
            //top wall-->row[minr] remains constant
            for(int  i = minr, j = maxc; j >= minc && cnt < tne; j--)
            {
                System.out.println(a[i][j]);
                cnt++;
            }
            minr++;//handling corner elements
        }
        
    }

}