/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;



public class PrimeOptimized {
	public static void main (String[] args) {
	    
	    Scanner s = new Scanner(System.in);
	    int n = s.nextInt();
	    
	    boolean isPrime = true;
	    
	    for(int i = 2; i*i <= n; i++ ) //1 < factor < sqrt(n)..eg: n:35, sqrt(35):5.9,35-->5,7. so we get 5.
	    {
	        if(n % i == 0)
	        {
	            isPrime = false;
	            break; 
	        }
	    }
	    
	    if(n < 2) isPrime = false;
	    
	    System.out.println(isPrime);
	}
}