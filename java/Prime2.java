/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;



public class Prime2 {
	public static void main (String[] args) {
	    
	    Scanner s = new Scanner(System.in);
	    int n = s.nextInt();
	    
	    boolean isPrime = true;
	    
	    for(int i = 2; i < n; i++ )
	    {
	        if(n % i == 0)
	        {
	            isPrime = false;
	            break; 
	        }
	    }
	    
	    if(n < 2) isPrime = false;
	    
	    System.out.println(isPrime);
	}
}