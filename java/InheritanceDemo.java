/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
//when you compile there would be 3 class files
class A
{
    public A()
    {
        System.out.println("in A");
    }
     public A(int i)
    {
        System.out.println("in A int");
    }
}
 //super(); ---> every subclass's constructor has this, even if you don't add it[compiler adds it].--calls constructor of the super class.
 
class B extends A
{
    public B()
    {
         //super();
        System.out.println("in B");
    }
    public B(int i)
    {
        super(i);// To call the parameterized constructor of the superclass
        System.out.println("in B int");
    }
}
public class InheritanceDemo{ 
	public static void main (String[] args) {
		//code
	   B obj1 = new B(5);
	   
	   //when you create object of subclass,it calls constructors of both super & sub class.
	   
	   //whenever you create object of subclass by specifying the parameter, it will call the specified constructor of the subclass & default 
	   //constructor of the superclass[not the parameterized constructor in superclass]. To change that refer to line 29.
	   
	   //@Override-> Using this annotation will give you a compile-time error instead of a logical error.
	}
}