import java.io.*;
import java.util.*;
/*
    lch - 'a' = uch - 'A'
    
    lch = 'a' + uch - 'A'
    
    uch = 'A' + lch - 'a'
	
	pepCOD = PEPcod
*/
public class ToggleString{

	public static String toggleCase(String str){
		//write your code here
        StringBuilder sb = new StringBuilder(str);
        
        for(int i = 0; i < sb.length(); i++)
        {
            char ch = sb.charAt(i);
            
            if(ch >= 'a' && ch <= 'z')//if ch is lowercase
            {
                char uch = (char)('A' + ch - 'a');
                sb.setCharAt(i,uch);
            }
            else
            {
                char lch = (char)('a' + ch - 'A');
                sb.setCharAt(i,lch);
            }
        }
        
        
		return sb.toString();
	}
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		String str = scn.next();
		System.out.println(toggleCase(str));
	}

}