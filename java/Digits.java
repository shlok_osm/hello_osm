import java.util.*;
  
  //Count number of digits in a number n.
  
  public class Digits{
  
  public static void main(String[] args) {
    // write your code here  
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    
    int num = (int) Math.log10(n) + 1;
    
    System.out.println(num);
   }
  }