/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

//Value equal to index value 

class Index {
	public static void main (String[] args) {
		//code
		Scanner s = new Scanner(System.in);
		int t = s.nextInt();
		while(t-- > 0) {
		    int n = s.nextInt();
		    int a[] = new int[n];
		    for(int i = 0; i < n; i++) {
		        a[i] = s.nextInt();
		    }
		    
		    boolean exists = false;
		    for(int i = 0; i < n; i++) {
		        if(a[i]==i+1) {
		           System.out.print(a[i] + " ");
		            exists = true;
		           }
		        else {
		            continue;
		          }
		    }
		    if(exists == false) System.out.print("Not Found");
		    System.out.println();
		}
		
	}
}