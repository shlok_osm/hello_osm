/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
//when you compile there would be 3 class files
class Student
{
    int rollno;
    String name;
}   
 
public class ArrayDemo {
	public static void main (String[] args) {
		//code
	   Student s1 = new Student();
	   Student s2 = new Student();
	   Student s3 = new Student();
	   Student s4 = new Student();
	   //Array of objects...
	   Student s[] = {s1,s2,s3,s4};
	   
	   /*int d[][] = {
	                    {1,2,3,4},
	                    {2,4,6,8},
	                    {5,6,7,8}
	   };
	   
	   for(int i = 0; i < 3; i++) //-->row
	   {
	       for(int j = 0; j < 4; j++) //-->column
	       {
	           System.out,print(" " + d[i][j]);
	       }
	       System.out.println();//printing every row in a new line
	   }
	   */
	   //Jagged Array: If number of columns of array is not fixed
	   
	   /*int d[][] = {
	                    {1,2,3,4},
	                    {2,4,6},
	                    {5,6,7,8,9}
	   };
	   
	   for(int i = 0; i < d.length; i++) //-->row...note: d.length
	   {
	       for(int j = 0; j < d[i].length; j++) //-->column...note: d[i].length
	       {
	           System.out.print(" " + d[i][j]);
	       }
	       System.out.println();//printing every row in a new line
	   }*/
	   
	   //Enhanced for-loop-->for-each loop
	   int a[] = {1,2,3,4};
	   for(int k : a)
		{
		   System.out.println(k);// note: k is not an index. k is the element.
		}
		
		
		int d[][] = {
	                    {1,2,3,4},
	                    {2,4,6},
	                    {5,6,7,8,9}
	   };
	   
	   for(int k[] : d) //note: k[]
		{
			for(int l :k)
			 {
				 System.out.print(" " + l);
			 }
			 System.out.println();
		}
				
	}
}