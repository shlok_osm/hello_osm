import java.io.*;
import java.util.*;
/*
	5
	4
	3
	2
	1
*/
public class RecusiveFunction1{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        printDecreasing(n);
    }

    public static void printDecreasing(int n){
        if(n > 0)
        {
            System.out.println(n);
            printDecreasing(n-1);
        }
    }

}