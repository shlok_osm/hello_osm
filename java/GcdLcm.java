import java.util.*;
    
	/*
		24 |36| 1
			24
		   ----
		    12 |24| 2
				24
			   ----
				0
				
		GCD = 12
		
		GCD * LCM = a * b
	*/
    public class GcdLcm{
    
    public static void main(String[] args) {
      // write your code here  
      Scanner s = new Scanner(System.in);
      int a = s.nextInt();
      int b = s.nextInt();
      
      int on1 = a;//original number 1
      int on2 = b;//original number 2
      
      while( a % b != 0)
      {
          int rem = a % b;
           a = b;
           b = rem;
      }
      
      int gcd = b;
      int lcm = (on1 * on2)/gcd;
      
      System.out.println(gcd);
      System.out.println(lcm);
     }
    }