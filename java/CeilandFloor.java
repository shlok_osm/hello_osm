import java.io.*;
import java.util.*;

public class CeilandFloor{

public static void main(String[] args) throws Exception {
    // write your code here
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    
    int[] a = new int[n];
    
    for(int i = 0; i < n; i++)
    {
        a[i] = s.nextInt();
    }
    
    int d = s.nextInt();
    
    int l = 0;
    int h = a.length-1;
    int ceil = 0;
    int floor = 0;
    
    while(l <= h)
    {
        int m = (l + h) / 2;
        if(d > a[m])
        {
            l = m + 1;
            floor = a[m];
        }
        else if(d < a[m])
        {
            h = m - 1;
            ceil = a[m];
        }
        else
        {
            ceil = a[m];
            floor = a[m];
            break;
        }
    }
    System.out.println(ceil);
    System.out.println(floor);
   // System.out.println(-1);
    
    
 }

}