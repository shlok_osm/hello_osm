import java.io.*;
import java.util.*;

//Print all substrings
/*
	s = "abcd"
		 0123 <--Indices
		 
	Substrings:
	
	0 1 a			1 2 b		2 3 c    3 4 d
	0 2 ab			1 3 bc		2 4 cd   i j
	0 3 abc			1 4 bcd		i j
	0 4 abcd		i j
	i j
	
	NOTE: 1. j = i+1
		  2. i is fixed, but also increments as 0 to s.length-1
		  3. j increments as i+1 to s.length
*/

public class PrintallSubstrings{

public static void main(String[] args) throws Exception {
    // write your code here
	String s = "abcd";
    for(int i = 0; i < s.length(); i++)
	{
		for(int j = i+1; j <= s.length(); j++)
		{
			System.out.println(s.substring(i,j));
		}
	}
 }

}