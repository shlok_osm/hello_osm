import java.util.*;
    
    public class Digitsofnumber{
    
    public static void main(String[] args) {
      // write your code here  
      Scanner s = new Scanner(System.in);
      int n = s.nextInt();
	//Another way...  
    //   int temp = n;
    //   int c = 0;
    //   while(temp != 0)
    //   {
    //       temp /= 10;
    //       c++;
    //   }
      int num = (int) Math.log10(n);
      int div = (int) Math.pow(10, num);
      while(div != 0)
      {
          int q = n/div;
          System.out.println(q);
          
          n = n % div;// 754 % 100-->54; 54 % 10-->4; 4%1--->0 
          div /= 10;
      }
     /*
	 Printing q...
          n=754
     div    rem     q
            
     100     54     7
     10      4      5
     1       0      4
     
          n=7600--->Edge Case
    div    rem     q      
    1000    600    7
    100     0      6
    10      0      0
    1       0      0
    
    
    */
     }
    }