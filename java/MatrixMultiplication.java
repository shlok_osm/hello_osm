import java.io.*;
import java.util.*;
/*
		 a1                    a2      					prd
		 
	[A11 A12 A13]   *   [B11 B12 B13 B14]         [C11 C12 C13 C14]
	[A21 A22 A23]		[B21 B22 B23 B24]    =    [C21 C22 C23 C24]
 2x3      			    [B31 B32 B33 B34]	   2x4
					 3x4
					 
	n1 = 2			n2 = 3
	m1 = 3			m2 = 4
	
	For any 2 matrices to be multiplied, the 1st matrix's column should be equal to the 2nd matrix's row i.e. m1 = n2.And the product matrix would be 
	of [2x4] i.e. [n1 x m2]
*/
public class MatrixMultiplication{

public static void main(String[] args) throws Exception {
    // write your code here
    Scanner s = new Scanner(System.in);
    int n1 = s.nextInt();
    int m1 = s.nextInt();
    
    int[][] a1 = new int[n1][m1];
    
    for(int i = 0; i < a1.length; i++)
    {
        for(int j = 0; j < a1[0].length; j++)
        {
            a1[i][j] = s.nextInt();
        }
    }
    
    
    int n2 = s.nextInt();
    int m2 = s.nextInt();
    
    int[][] a2 = new int[n2][m2];
    
    for(int i = 0; i < a2.length; i++)
    {
        for(int j = 0; j < a2[0].length; j++)
        {
            a2[i][j] = s.nextInt();
        }
    }
    //c1=r2
    //m1=n2
    if(m1 != n2)
    {
        System.out.println("Invalid input");
        return;
    }
    
        int prd[][] = new int[n1][m2];
    
        for(int i = 0; i < prd.length; i++)
        {
            for(int j = 0; j < prd[0].length; j++)
            {
                for(int k = 0; k < m1; k++)//or can be k < n2.....variable of a1 & a2 matches i.e. m1=n2
                {									 //Fixed: '-' ; Variable: '.'
                    prd[i][j] += a1[i][k] * a2[k][j];//Eg.:C11 = A11*B11 + A12*B21 + A13*B31----->Row of a1 multiplied to a column of a2.
                }									 //           -.  .-    -.  .-    -.  .-
            }										//C11-->1: a1's row is fixed i.e. 1[A11,A12,A13], all columns vary
        }   										//----->1: a2's column is fixed i.e. 1[B11,B21,B31], all rows vary
    
        for(int i = 0; i < prd.length ; i++)
        {
            for(int j = 0; j < prd[0].length; j++)
            {
                System.out.print(prd[i][j] + " ");
            }
            System.out.println();
        }   
    
    
 }

}