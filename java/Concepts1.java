/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;


class Concepts1 {
	public static void main (String[] args) {
		//code
		
		//continue...
		
		/*for(int i=1;i<=10;i++) {
		    if(i==7) {
		        continue;
		    }
		   System.out.println("value:" + i); 
		}*/ 
		
		//break...
		
		for(int i=1;i<=10;i++) {
		    if(i>5) {
		        break;
		    }
		    System.out.println("value:" + i);
		}
		
	}
}
