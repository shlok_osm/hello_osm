/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;

//To print..
/*
        ----1--
        ---2-3--
        --4-5-6--
        -7-8-9-10--
        11-12-13-14-15--
*/

public class Pattern3 {
	public static void main (String[] args) {
	    
	   Scanner s = new Scanner(System.in);
	   int n = s.nextInt();
	    
	    int num = 1;
	    for(int i = 1; i <= n; i++)
	    {
	        String s1 = "";
	        for(int j = 1; j <= n-i; j++){
	            s1 = s1 + "  ";
	            //System.out.print( "*"  + " ");
	        }
	        for(int j = 1; j <= i; j++)
	        {
	            s1 = s1 + num++ + "  ";
	        }
	        System.out.println(s1);
	    }
	   
	    
	}
}