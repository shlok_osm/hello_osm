import java.io.*;
import java.util.*;

// https://pasteboard.co/JysQpiZ.png

// https://pasteboard.co/JysRNFu.png

// https://pasteboard.co/JysSzDR.png

public class GetMazepaths{

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int m = s.nextInt();
        
        ArrayList<String> paths = getMazePaths(1,1,n,m);
        System.out.println(paths);
        
    }

    // sr - source row --> 1
    // sc - source column --> 1
    // dr - destination row --> n
    // dc - destination column --> m
    public static ArrayList<String> getMazePaths(int sr, int sc, int dr, int dc) {
        if(sr == dr && sc == dc)
        {
            ArrayList<String> bres = new ArrayList<>();
            bres.add("");
            return bres;
        }
        
        ArrayList<String> hpaths = new ArrayList<>();
        ArrayList<String> vpaths = new ArrayList<>();
        
        if(sc < dc)
        {
             hpaths = getMazePaths(sr,sc+1,dr,dc);
        }
        if(sr < dr)
        {
            vpaths = getMazePaths(sr+1,sc,dr,dc);
        }
        
         ArrayList<String> paths = new ArrayList<>();
         
        for(String hpath : hpaths)
        {
            paths.add("h" + hpath);
        }
        
        for(String vpath : vpaths)
        {
            paths.add("v" + vpath);
        }
        
        return paths;
    }

}