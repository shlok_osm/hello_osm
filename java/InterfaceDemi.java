/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;

//Abstract class -> Define and declare
//Interface -> Only declare -> till java 1.7
//From Java 1.8 -> can also define methods in interface --> using default

interface Demo
{
    void abc();
    default void show()
    {
        System.out.println("in show");
    }
}

class DemoImp implements Demo
{
    public void abc()
    {
        System.out.println("in abc");
    }
    /* Method Overriding possible...
    public void show()
    {
        System.out.println("in New Show");
    }*/
}

public class InterfaceDemi {
	public static void main (String[] args) {
	   Demo obj = new DemoImp();
	   obj.abc();
	   obj.show();
	}
}