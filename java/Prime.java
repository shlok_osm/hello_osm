/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;



public class Prime {
	public static void main (String[] args) {
	    
	    Scanner s = new Scanner(System.in);
	    int n = s.nextInt();
	    int c = 0;
	    
	    for(int i = 1; i <= n; i++)// If i=0, there would be Arithemetic exception: n%0
	    {
	        if(n%i==0)
	        {
	            c++;
	        }
	    }
	    if(c==2)
	    {
	        System.out.println("Prime number");
	    }
	    else
	    {
	        System.out.println("Not Prime number");
	    }
	}
}