import java.util.*;
  /*
	Prime optimized....
	NOTE: If a number is "not prime", it will be divided by any number in the range of 2 to the squareroot of the number.
			2 < factor < squareroot[number]
			     OR
			2 < factor < √n	 
	Q) Why 1 is not in the range?
	A-> Because 1 divides every number
	
	Examples:
	1. number = 24					2. number = 25					3. number = 17
		1 * 24 <---> 24 * 1				1 * 25							1 * 17
		2 * 12 <---> 12 * 2				5 * 5							17 * 1
		3 * 8  <---> 8  * 3				25 * 1							[prime,Therefore not able to divide till it's squareroot]
		4 * 6  <---> 6  * 4				[see, 25 is divided till 5 
		 √[24] = 4.89			 		 which is it's squareroot] 
		[see, 24 is divided till 4		 √[25] = 5
		 which is it's approx 
		 squareroot]
		 
		 
	div  	  <= √n
	div² 	  <= n
	div * div <= n
		 
  */
  public class Prime3{
  
  public static void main(String[] args) {
      Scanner scn = new Scanner(System.in);
       int t = scn.nextInt();
       
       while(t-- > 0)
       {
           int n = scn.nextInt();
           
           int c = 0;
           for(int div = 2; div * div <= n; div++)//   div <= √n
           {
               if(n % div == 0)
               {
                   c++;
                   break; //Example = 12...starting from 2 till √[12] = 3.4[approx]...12 % 2 == 0...here in the start itself 2 is divied by 12, 
						  //then why to check till its squareroot
               }
           }
           
           if(c == 0)
           {
               System.out.println("prime");
           }
           else
           {
               System.out.println("not prime");
           }
       }
       // write ur code here//
  
   }
  }