/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;

//Static keyword & Interfaces...

interface Demo
{
    void abc();
    static void show()
    {
        System.out.println("hi");
    }
}


public class InterfacesDimo {
	public static void main (String[] args) {
	   Demo.show();
	}
}