import java.io.*;
import java.util.*;
/*
Floor=    7          *
          6          *
          5          *
          4          *  *
          3 *        *  *
          2 *        *  *
          1 *  *     *  *
            3  1  0  7  5   -->Buildings
            
            a.length--> For every floor[starting from max], we are 
                        deciding whether there will be "*" or not.
*/
public class Barchart{

public static void main(String[] args) throws Exception {
    // write your code here
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    
    int a[] = new int[n];
    
    for(int i = 0; i < a.length; i++)
    {
        a[i] = s.nextInt();
    }
    
    //Finding the max element..
    int max = a[0];
    for(int i = 1; i < a.length; i++)
    {
        if(a[i] > max)
        {
             max = a[i];
        }
    }
    
    for(int floor = max; floor >= 1; floor--)
    {
        
        for(int i = 0; i < a.length; i++)
        {
            if(a[i] >= floor)
            {
                System.out.print("*\t");
            }
            else
            {
                System.out.print("\t");
            }
        }
        System.out.println();
    }
 }

}