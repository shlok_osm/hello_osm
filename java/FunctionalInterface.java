/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;

/* Types of interface...
1. Normal interface: If the interface has more than 1 method.
2. Single abstract method interface[SAM]: It will have only 1 method --> From java 8 onwards, it is Functional interface --> Lambda Expression -- Scala[Java 8 adopted all features which were there in scala]
3. Marker interface: does not have any method. E.g.: Serializable
*/
//@functionalInterface//if you add 1 more method in the below interface, there will be an error. 
interface Abc 
{
    void show();
}


public class FunctionalInterface {
	public static void main (String[] args) {
	    Abc obj = () -> System.out.println("anything"); // Creating interface object in 1 line. --> bracket, arrow, method defination --> Lambda expression --> only possible with functional interfaces which only works in Java 8.
	    obj.show();
		
	}
}