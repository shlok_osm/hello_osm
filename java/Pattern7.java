import java.util.*;

public class Pattern7 {

    public static void main(String[] args) {
        // Write your code here
        /*
            *****
            ---*-   i=2, -=3  5-i=5-2=3
            --*--   i=3, -=2  5-i=5-3=2
            -*---   i=4, -=1  5-i=5-4=1
            *****
            
            
        */    
        
        for(int i = 1; i <=5; i++)
        {
           StringBuilder s = new StringBuilder("");//
           //String s = "";
            if(i==1 || i==5)
            {
                for(int j=1; j <= 5; j++)
                {
                   //s = s + "*";
                   s.append("*");
                }
            }
            else
            {
                for(int j=1; j <= 5-i; j++)
                {
                  //s = s + " "; 
                   s.append(" ");
                }
               // s = s + "*";
               s.append("*");
            }
            System.out.println(s);
        }
        
        // System.out.println("*****");
        // System.out.println("   * ");
        // System.out.println("  *  ");
        // System.out.println(" *   ");
        // System.out.println("*****");
    }
}