/*package whatever //do not write package name here 

Input:
2--->Testcases
3--->Array size
3 2 1--->Array elements
4
1 2 3 4
Output:
6
10
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class Sum {
    static void sum(int[] a){
        int sum = 0;
         for(int i=0;i<a.length;i++)
	            sum += a[i];
	   System.out.println(sum);
	   
    }
	public static void main (String[] args) {
	    Scanner s = new Scanner(System.in);
	    int t = s.nextInt();
	    while(t-- > 0){
	       int n = s.nextInt();
	       int[] a = new int[n];
	       for(int i=0;i<n;i++)// a.length
	            a[i] = s.nextInt();
	       sum(a);
	    }
		
	}
}