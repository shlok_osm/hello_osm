import java.io.*;
import java.util.*;
/*
    abc --> Length = 3 --> no. of permutations = 3x2x1 = 6
    abc bac cab acb bca cba
     0   1   2   3   4   5 <--- f
    
    Since n = 3 till n = 1...
                        0 1 2
                        a b c
    0 = 3|0   
        2|0-0   a       b c
        1|0-0   ab      c
        |0-0    abc
        
    Simillarly, 1, 2, 3, 4, 5 will have their representations    
*/
/*
	Print All Permutations Of A String Iteratively
*/
public class StringPermutations{

    public static int factorial(int n)
    {
        int val = 1;
        for(int i = 2; i <= n; i++)
        {
            val = val * i;
        }
        return val;
    }
    
	public static void solution(String str){
		// write your code here
		int n = str.length();
		int f = factorial(n);
		
		for(int i = 0; i < f; i++)
		{
		    StringBuilder sb = new StringBuilder(str);
		    int temp = i;
		    
		    for(int div = n; div >= 1; div--)
		    {
		        int q = temp / div;
		        int r = temp % div;
		        
		        System.out.print(sb.charAt(r));
		        sb.deleteCharAt(r);
		        
		        temp = q;
		    }
		    System.out.println();
		}
	}
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		String str = scn.next();
		solution(str);
	}

}