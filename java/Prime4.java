import java.util.*;
  /*
	This time did it without "int c = 0;", thus saving memory
	
	div  	  <= √n
	div² 	  <= n
	div * div <= n
  */
  public class Prime4{
  
  public static void main(String[] args) {
      Scanner scn = new Scanner(System.in);
       int t = scn.nextInt();
       
       while(t-- > 0)
       {
           int n = scn.nextInt();
           
          int div = 2;
          while( div * div <= n ) //div <= √n
           {
               if(n % div == 0)
               {
                  break;
               }
               div++;
           }
           
           if(div * div > n)// div > √n --> If a number n is prime, div will increase till √n and eventually exceed it i.e. no div from 2 to √n was 
							//able to divide n
           {
               System.out.println("prime");
           }
           else
           {
               System.out.println("not prime");
           }
       }
       // write ur code here//
  
   }
  }