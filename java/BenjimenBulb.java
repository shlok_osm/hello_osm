import java.util.*;
  
  public class BenjimenBulb{
  
  public static void main(String[] args) {
    // write your code here  
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    //Printing perfect squares as they have odd no. of factors for which the bulb glows
    for(int i = 1; i*i <= n; i++)
    {
        System.out.println(i*i);
    }
   }
  }