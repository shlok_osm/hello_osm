import java.io.*;
import java.util.*;
/*
    3
    2
    1
    1
    2
    3
    
    E   |  F
--------|-------
    3   | Sop(3)
    2   | 2
    1   | 1
    1   | 1
    2   | 2
    3   | Sop(3)
    
    Sopln(n);
    Pdi(n-1);
    Sopln(n);
*/
public class Recursion3{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        pdi(n);
    }

    public static void pdi(int n){
        if(n > 0)
        {
            System.out.println(n);
            pdi(n-1);
            System.out.println(n);
        }
    }

}