/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

interface Abc 
{
    void show();
}

class Implimentor implements Abc
{
    public void show()
    {
        System.out.println("anything");
    }
}
public class InterfaceDemo {
	public static void main (String[] args) {
	    Abc obj = new Implimentor(); //Reference of interface, object of class
	    obj.show();
		
	}
}