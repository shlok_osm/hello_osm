import java.io.*;
import java.util.*;

//Remove primes from Arraylist
//Trick: run loop backwards...Why?
/*
    Loop Forward:
    7 18 3 14 19 31 72
    0 1  2 3  4  5  6
    
    i=0-->removed 7[as was prime]
    i=0 is now 18 & since we're doing i++, we go to i=1 i.e.3,
    thus completely ignoring 18.
    
    Loop backwards:
    7 18 3 14 19 31 72
    0 1  2 3  4  5  6
    
    i=6-->72--> not prime-->i--
    i=5-->31-->remove
    now i=5 is 72, thus not affecting elements before the deleted
    elements
*/
public class RemoveprimesfromArraylist{

    public static boolean isPrime(int val)
    {
        for(int div = 2; div * div <= val; div++)
        {
            if(val % div == 0)
            {
                return false;
            }
        }
        return true;
    }

	public static void solution(ArrayList<Integer> al){
		// write your code here
		for(int i = al.size()-1; i >= 0; i--)
		{
		    int val = al.get(i);
		    if(isPrime(val) == true)
		    {
		        al.remove(i);
		    }
		}
		
	}
	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		int n = scn.nextInt();
		ArrayList<Integer> al = new ArrayList<>();
		for(int i = 0 ; i < n; i++){
			al.add(scn.nextInt());
		}
		solution(al);
		System.out.println(al);
	}

}