/*package whatever //do not write package name here 
Creating object of interface using anonymous class*/

import java.util.*;
import java.lang.*;
import java.io.*;

interface Abc 
{
    void show();
}


public class InterfaceDemoo {
	public static void main (String[] args) {
	    Abc obj = new Abc()
					{
						public void show()
						{
							System.out.println("anything");
						}
					};
	    obj.show();
		
	}
}