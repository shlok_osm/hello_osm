/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

class Printer
{
	//Reasons why we need an abstract class
    //Number is an abstract class.--> 1.We never create object of Number class. we create objects of Integer,Double,etc.
    //2. Instead of wasting 2 methods, we can use 1 method which accepts all the subclass objects.
    //Integer and Double both extends Number class and have their implementations accordingly.
    
    /*
    public void show(Integer i)
    {
        System.out.println(i);
    }
    public void show(Double i)
    {
        System.out.println(i);
    }
    */
    //Direct implementation
    public void show(Number i)
    {
        System.out.println(i);
    }
}

public class AbstractDemoo {
    
	public static void main (String[] args) {
		//code
	   Printer obj = new Printer();
	   obj.show(5.5);
	}
}