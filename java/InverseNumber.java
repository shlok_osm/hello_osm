import java.util.*;

/*
  Inverse of a number
  
  p->position, d->digit
  
  p-> 5 4 3 2 1    p   d
  d-> 2 1 4 5 3
  
  21453 % 10 = 3   1   3
  2145 % 10 =  5   2   5
  214 % 10 =   4   3   4
  21 % 10 =    1   4   1
  2 % 10 =     2   5   2
  
                ---Inverse---
                p-> 5 4 3 2 1
                d-> 2 3 1 5 4
                   p   d
                   1   4
                   2   5    d*10^[p-1]
                   3   1
                   4   3    + 3*10^3    
                   5   2    + 2*10^4[To bring 2 from d in 5{from p} adding this]
  */


public class InverseNumber{

public static void main(String[] args) {
  // write your code here  
  Scanner s = new Scanner(System.in);
  int n = s.nextInt();
  
  int inv = 0;//inverse
  int op = 1;//original place
  while(n!=0)
  {
      int od = n % 10;//original digit
      int id = op;//inverted digit
      int ip = od;//inverted place
      
      
      inv = inv + id * (int) Math.pow(10,ip-1);
      
      n /= 10;
      op++;
  }
  System.out.println(inv);
  
  
 }
}