import java.io.*;
import java.util.*;
/*
    Find Element In An Array
    
    1.You are given a number n, representing the size of array a.
    2.You are given n numbers, representing elements of array a.
    3.You are given another number d.
    4.You are required to check if d number exists in the array a 
      and at what index (0 based). If found print the index, 
      otherwise print -1.
*/
public class ArrayIndex{

public static void main(String[] args) throws Exception {
    // write your code here
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    
    
    int a[] = new int[n];
    
    for(int i = 0; i < n; i++)
    {
        a[i] = s.nextInt();
    }
    
    int d = s.nextInt();
    
    int index = -1;
    for(int i = 0; i < n; i++)
    {
        if(a[i] == d)
        {
            index = i;
            break;
        }
    }
    System.out.print(index);
    
 }

}