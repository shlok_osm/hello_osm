import java.util.*;

//Prime numbers in a range

public class Primerange{
    public static void main(String[] args) {
        // write your code here
        Scanner s = new Scanner(System.in);
        int low = s.nextInt();
        int high = s.nextInt();
        
        
        for(int i = low; i <= high; i++)
        {
            int c = 0;
            for(int j = 2; j*j <= i; j++)
            {
                if(i%j==0)
                {
                    c++;
                    break;
                }
            }
            
            if(c==0)
            {
                System.out.println(i);
            }
        }
    }
}