/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;

//To print sum of--> 1 + 1/2 + 1/3 + 1/4 + 1/5....+ 1/n

public class SeriesSum {
	public static void main (String[] args) {
	    
	    Scanner s = new Scanner(System.in);
	    int n = s.nextInt();
	    
	    float res = 0;
	    
	    for(float i = 1; i <= n; i++)
	    {
	        res += 1/i;
	    }
	   
	    System.out.println(res);
	}
}