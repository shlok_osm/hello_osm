import java.io.*;
import java.util.*;
/*
    1. You are given a positive number n. 
    2. You are required to print the counting from 1 to n.
    
    Q). How to solve recurssion?
    A-> 1. High-level-view[HLV]      2. Low-level-view[LLV]
    
    HLV:                        
    1.Expectation[E]            
    2.Faith[F]
    3.F + E
	
	Expectation[E]   |  Faith[F]
---------------------|----------------
PI(n) = 1			 | PI(n-1) = 1
		2			 |			 2
		.			 |			 .
		.			 |			 .
		n			 |			n-1
		
	F + E :
		PI(n) = PI(n-1)
				Print n

				
	LLV:
	Dry run in stack
	
    NOTE : When you put a recursive call in a function, the code
           divides in 2 parts:
           a). 1st part i.e. code above the recursive call, is 
               executed when going upwards in stack. It is executed
               before the 2nd part.
           b). 2nd part i.e. code below/after the recursive call ,
                is executed when going downwards in stack. It is
                executed after the 1st part.
                
    Example:
    n = 5
        Only 1st part   Code after recursive 
            executed        call executed
    PI(1) -> 1              Print 1
    PI(2) -> 2              Print 2
    PI(3) -> 3              Print 3
    PI(4) -> 4              Print 4
    PI(5) -> 5              Print 5
    
    
*/
public class Recursion2{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        printIncreasing(n);
    }

    public static void printIncreasing(int n){
        if(n > 0)//stopping recurssion at n = 0
        {
            printIncreasing(n-1);
            System.out.println(n);
        }
        
    }

}