/*Complete the Function below

    Palindromic Array
    
    Input:
    2
    5
    111 222 333 444 555
    3
    121 131 20

    Output:
    1
    0
    
*/
class PalindromicArray
{   
    /*
    public static boolean checkPalindrome(int a){
                      int temp = a, remainder = 0, rev = 0;
                      while(a > 0) {
                             remainder = a % 10;
                             rev = (rev * 10) + remainder;
                             a /= 10;
                      }
                      if(temp == rev)
                        return true;
                    return false;
    }
	public static int palinArray(int[] a, int n)
           {
                  //add code here.
                  int rev=0, remainder=0,temp=0,index= 0;
                  boolean found[] = new boolean[n];
                  for(int x : a) {// i = a[0],a[1]
                      if(checkPalindrome(x) == false)
                            return 0;
                  }
              return 1;   
                  
            }*/
            public static boolean checkPalindrome(String s){// "113"
                char s1[] = s.toCharArray();//
                String s2 = "";
                for(int i = s.length()-1;i>=0;i--){// '1' '1' '3'
                    s2 = s2 + s1[i];//"311"
                }// e l p p a
                if(s2.equals(s))
                    return true;
                return false;
            }
            public static int palinArray(int[] a, int n)
           {
                for(int x: a) {
                    String s = ""+x;
                    if(checkPalindrome(s) == false)
                            return 0;
                  }
              return 1;   
            }
               
               
               
               
           
}