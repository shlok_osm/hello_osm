import java.io.*;
import java.util.*;

public class BinarySearchArray{

public static void main(String[] args) throws Exception {
    // write your code here
    //Binary search works only on sorted array
    int[] a = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    int data = 78;
    
    int l = 0;
    int h = a.length-1;
    while(l <= h)
    {
        int m = (l + h) / 2;
        if(data > a[m])
        {
            l = m + 1;
        }
        else if(data < a[m])
        {
            h = m - 1;
        }
        else
        {
            System.out.println(m);
            return;
        }
    }
    System.out.println(-1);
 }

}