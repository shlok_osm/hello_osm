/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

//int, float, double

//Integer,Float,etc.

//Primitive are faster than wrapper classes, but there are certain frameworks[hibernate,collection api] which only supports wrapper classes, 
//not primitives.

public class WrapperDemo {
    
	public static void main (String[] args) {
		//code
	   int i = 5; //Primitive datatype --> primitive variable
	   //Integer ii = new Integer(5); //Wrapper Class--> ii is a refernce variable-->you are storing 5 in an object
	   
	   Integer ii = new Integer(i); // Boxing - Wrapping
	   
	   Integer value = i; //Autoboxing --> Java automatically does the RHS of line 18 in the backend. when you do it, it's boxing.
	   
	   int j = ii.intValue(); // unboxing --> unwrapping
	   
	   int k = value; //autounboxing --> Java automatically does the RHS of line 22  in the backend. when you do it, it's unboxing.
	   
	   String str = "123";
	   
	   int n = Integer.parseInt(str); //parseInt is a static method, so we have to use the classname i.e. Integer
	   System.out.println(n);
	}
}