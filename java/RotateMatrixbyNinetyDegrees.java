import java.io.*;
import java.util.*;
/*
    a b c d         m i e a
    e f g h         n j f b
    i j k l----->   o k g c
    m n o p         p l h d
    
    Step-1: Transpose--> a e i m
                         b f j n
                         c g k o
                         d h l p
                         
    Step-2: Reverse each row:
            m i e a
            n j f b
            o k g c
            p l h d
*/
public class RotateMatrixbyNinetyDegrees{

    public static void main(String[] args) throws Exception {
        // write your code here
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
    
    
        int[][] arr = new int[n][n];
        
        for(int i = 0; i < arr.length; i++)
        {
            for(int j = 0; j < arr[0].length; j++)
            {
                arr[i][j] = s.nextInt();
            }
        }
    
        //Transpose
        for(int i = 0; i < arr.length; i++)
        {
            for(int j = i; j < arr[0].length; j++)//Traversal In upper triangle only
            {
                int temp = arr[i][j];
                arr[i][j] = arr[j][i];
                arr[j][i] = temp;
            }
        }
        
        //Reverse elements row by row...
        for(int i = 0; i < arr.length; i++)
        {
            int li = 0;
            int ri = arr[i].length-1;
            
            while(li < ri)
            {
                int temp = arr[i][li];
                arr[i][li] = arr[i][ri];
                arr[i][ri] = temp;
                
                li++;
                ri--;
            }
        }
        
        display(arr);
}

    public static void display(int[][] arr){
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr[0].length; j++){
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

}