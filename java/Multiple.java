/*package whatever //do not write package name here 

Calculate the product of all the elements in an array.

Input:
2--->Testcase
5--->Array size
1 2 3 4 5--->Array elements
10
5 5 5 5 5 5 5 5 5 5

Output:
120---1*2*3*4*5=120
9765625
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class Multiple {
	public static void main (String[] args) {
		//code
		Scanner s =  new Scanner(System.in);
		int t = s.nextInt();
		while(t-- >0) {
		    int n = s.nextInt();
		    int a[] = new int[n];
		    for(int i = 0; i < n; i++) {
		        a[i] = s.nextInt();
		    }
		    int product = 1;
		    for(int i = 0; i < n; i++) {
		        product *= a[i];
		    }
		    System.out.println(product);
		}
	}
}