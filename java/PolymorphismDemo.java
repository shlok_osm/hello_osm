/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
//when you compile there would be 3 class files
class A
{
    public void show()
	{
		System.out.println("in A");
	}
}
 
 
class B extends A
{
    public void show()
	{
	
		System.out.println("in B");
	}
	public void config()
	{
	    System.out.println("config");
	}
}

class C extends A
{
    public void show()
	{
	
		System.out.println("in C");
	}
}
public class PolymorphismDemo {
	public static void main (String[] args) {
		//code
	   A obj1 = new B(); // runtime polymorphism
	   obj1.show();// object is of B, so B's show will be called
	   //obj1.config; //gives error, as class A do not have config()
	   obj1 = new C();
	   obj1.show(); //dynamic method dispatch
	}
}