import java.io.*;
import java.util.*;

// https://pasteboard.co/JEjcxHT.png

// 1 to end --> unknown [As we transverse, elements go from unknown to known]
// 0 to j-1 --> <= pivot
// j to i-1 --> > pivot


// Applications of this question:
//     1. odd-even separation
//     2. 0-1 separate
//     3. 0-Non-zero separate
    
public class Partition {

  public static void partition(int[] arr, int pivot){
    //write your code here
    int i = 0;
    int j = 0;
    
    while(i < arr.length)
    {
        if(arr[i] > pivot)
        {
            i++; // effect[unknown elements getting shortened]
                 //effect [increase in '>' than elements ]
        }
        else
        {
            swap(arr, i, j); //effect [increase in '<=' than elements ]
            i++;            // effect[unknown elements getting shortened]
            j++;           
        }
    }
  }

  // used for swapping ith and jth elements of array
  public static void swap(int[] arr, int i, int j) {
    System.out.println("Swapping " + arr[i] + " and " + arr[j]);
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }

  public static void print(int[] arr) {
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i] + " ");
    }
    System.out.println();
  }

  public static void main(String[] args) throws Exception {
    Scanner scn = new Scanner(System.in);
    int n = scn.nextInt();
    int[] arr = new int[n];
    for (int i = 0; i < n; i++) {
      arr[i] = scn.nextInt();
    }
    int pivot = scn.nextInt();
    partition(arr,pivot);
    print(arr);
  }

}