import java.io.*;
import java.util.*;
/*
	Subset of n elements is 2^n
	Subset of a,b,c will be:
			0 - 0 0 0----------> - - -
			1 - 0 0 1----------> - - c
			2 - 0 1 0----------> - b -
			3 - 0 1 1----------> - b c
			4 - 1 0 0----------> a - -
			5 - 1 0 1----------> a - c
			6 - 1 1 0----------> a b -
			7 - 1 1 1----------> a b c
*/
public class Main{

public static void SubsetArray(String[] args) throws Exception {
    // write your code here
    Scanner s = new Scanner(System.in);
    int n = s.nextInt();
    
    int[] a = new int[n];
    
    for(int i = 0; i < n; i++)
    {
        a[i] = s.nextInt();
    }
    
    int limit = (int)Math.pow(2,n);
    for(int i = 0; i < limit; i++)
    {
    //Convert i to binary & use 0's and 1's to know if to print the 
    //element or not
        String set = "";
        int temp = i;
        
        for(int j = n-1; j >= 0 ; j--)
        {
            int r = temp % 2;
            temp = temp / 2;
            
            if(r == 0)
            {
                set = "-\t" + set;
            }
            else
            {
                set = a[j] + "\t" + set; 
            }
        }
        System.out.println(set);
    }
 }

}