/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;

class A
{
    public void show()
    {
        System.out.println("in A show");
    }
}


public class AnonymousExample {
	public static void main (String[] args) {
	    A obj = new A()
	            {
	                public void show()
	                {
	                    System.out.println("hello");
	                }
	            };// This is a class which does not have a name.
	            // Purpose was to override class A's method, which anonymous class achieved.
	            //Cannot reuse this class --> only for 1-time use.
				//Normally, we create a class then it's object. In anonymous class, we first create the object and then specify it's implementation.
	    obj.show();
		
	}
}