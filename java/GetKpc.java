import java.io.*;
import java.util.*;

// https://pasteboard.co/JAwhtut.png

// https://pasteboard.co/JAwi5kzZ.png

// https://pasteboard.co/JAwiDcy.png

// Input1 - 678; Input2 = 78

// Output2 = [tv, tw, tx, uv, uw, ux]

public class GetKpc {

    public static void main(String[] args) throws Exception {
        Scanner s = new Scanner(System.in);
        String str = s.nextLine();
        ArrayList<String> words = getKPC(str);
        System.out.println(words);
    }

    static String[] codes = {".;","abc","def","ghi","jkl","mno","pqrs","tu","vwx","yz"};
    
    public static ArrayList<String> getKPC(String str) {
        // Base-case
        if(str.length() == 0)
        {
             ArrayList<String> bres = new ArrayList<>();
             bres.add("");
             return bres;
        }
        
        char ch = str.charAt(0);
        String ros = str.substring(1);
        
        ArrayList<String> rres = getKPC(ros); //[tv, tw, tx, uv, uw, ux]
        ArrayList<String> mres = new ArrayList<>();
                                                                                    //0 1 2 3        
        String codeforch = codes[ch - '0']; //codeforch = codes[54 - 48] = codes[6] = p q r s
        for(int i = 0; i < codeforch.length(); i++)
        {
            char chcode = codeforch.charAt(i);
            
            for(String rstr : rres)
            {
                mres.add(chcode + rstr);
            }
        }
        return mres;
    }

}