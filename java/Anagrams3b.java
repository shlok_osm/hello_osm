/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;

//Incrementing in 1 string, decrementing in other in same array, if all values in array = 0, then anagram.
//Best method...
public class Anagrams3b {
	public static void main (String[] args) {
	    
	    String a = "act";
	    String b = "cat";
	    
	    boolean isAnagram = true;
	   	int al[] = new int[256]; // Initially, 0.
	    
	    
	    for(char c: a.toCharArray())   
	    {
	        int index = (int) c; // a -- > 97; c --> 99; t--> 116[got collected in index]
	        al[index]++;
	    }
	    
	    for(char c: b.toCharArray())
	    {
	        int index = (int) c; 
	        al[index]--;
	    }
	    
	    for(int i = 0; i< 256; i++)
	    {
	        if(al[i] != 0)
	        {
	            isAnagram = false;
	            break;
	        }
	    }
	    
	    if(isAnagram) 
	    {
	        System.out.println("anagram");
	    }
	    else
	    {
	        System.out.println("not anagram");
	    }
	}
}

