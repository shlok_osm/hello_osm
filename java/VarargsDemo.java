/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
//when you compile there would be 3 class files
class Calc 
{
    public int add(int ... n) // {4,5,6,7,8,9,6}---> Variable length Arguments
    {
        int sum = 0;
        for(int i : n)
        {
            sum += i;
        }
        return sum;
    }
}
 
public class VarargsDemo{
	public static void main (String[] args) {
		//code
	  Calc obj = new Calc();
	  System.out.println(obj.add(4,5,6,7,8,9,6));
	   
	}
}