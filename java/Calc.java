/*package whatever //do not write package name here 
	Object creation & constructors*/

import java.util.*;
import java.lang.*;
import java.io.*;

class Calc
{
    int num1;
    int num2;
    int result;
    
    public Calc() {
        num1 = 5;
        num2 = 10;
        System.out.println("in constructor");
    }
    
    public Calc(int n) {
        num1 =n;
    }
     
    public Calc(double d) {
        num1 = (int) d;
    }
}
 
class GFG {
	public static void main (String[] args) {
		//code
		Calc obj = new Calc(7.5);
		System.out.println(obj.num1);
	}
}