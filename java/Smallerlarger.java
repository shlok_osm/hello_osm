/*package whatever //do not write package name here 

Given a sorted array and a value x, find the number of array elements less than or equal to x and elements more than or equal to x. 

Input:
3
7 0
1 2 8 10 11 12 19
7 5
1 2 8 10 11 12 19
7 10
1 2 8 10 11 12 19

Output:
0 7
2 5
4 4
*/

import java.util.*;
import java.lang.*;
import java.io.*;

class Smallerlarger {
	public static void main (String[] args) {
		//code
		Scanner s = new Scanner(System.in);
		int t = s.nextInt();
		while(t-- > 0) {
		    int n = s.nextInt();
		    int x = s.nextInt();
		    int a[] = new int[n];
		    for(int i = 0; i < n; i++) {
		        a[i] = s.nextInt();
		    }
		    int c1 = 0;
		    int c2 = 0;
		    for(int i = 0; i < n; i++) {
		        if(a[i] <= x) {
		            c1++;
		        }
		        if(a[i] >= x) {
		            c2++;
		        }
		    }
		    System.out.println(c1 + " " + c2);
		}
	}
}