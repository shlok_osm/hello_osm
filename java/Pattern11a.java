import java.util.*;
/*
    ----*                    
    ---**
    --***
    -****
    *****
*/
public class Pattern11a {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        int n = scn.nextInt();
        // write ur code here
        
        for(int i = 1; i <= n; i++)
        {
            StringBuilder s = new StringBuilder("");
            for(int j =1; j <= n-i; j++)
            {
                s.append("\t");
            }
            
            for(int j = 1; j <= i; j++)
            {
                s.append("*\t");
            }
            System.out.println(s);
        }

    }
}