/*package whatever //do not write package name here 
*/

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.Scanner;

//To print --> x^y

public class Exponents {
	public static void main (String[] args) {
	    
	    Scanner s = new Scanner(System.in);
	    int x = s.nextInt();
	    int y = s.nextInt();
	    int res = 1;
	    for(int i = 0; i < y; i++ )
	    {
	        res *= x;
	    }
	    System.out.println(res);
	    
	}
}