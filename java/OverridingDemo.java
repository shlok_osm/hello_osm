/*package whatever //do not write package name here */

import java.util.*;
import java.lang.*;
import java.io.*;
//when you compile there would be 3 class files
class A
{
	int i;
    public void show()
	{
		System.out.println("in A");
	}
}
 
 
class B extends A
{
	int i;
    public void show()
	{
		super.i = 8; //Assigning the value '8' to class A's i.
		super.show(); // To refer parent class method
		System.out.println("in B");
	}
}
public class OverridingDemo {
	public static void main (String[] args) {
		//code
	   B obj1 = new B();
	   obj1.show();
	}
}