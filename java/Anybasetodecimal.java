import java.util.*;
  
  public class Anybasetodecimal{
  
  public static void main(String[] args) {
      Scanner scn = new Scanner(System.in);
      int n = scn.nextInt();
      int b = scn.nextInt();
      int d = getValueIndecimal(n, b);
      System.out.println(d);
   }
  
   public static int getValueIndecimal(int n, int b){
      // My way...
      int c = 0;
      int count = 0;
      while(n!=0)
      {
          int r = (n % 10) * (int)Math.pow(b,count);
          n /= 10;
          c += r;
          count++;
      }
      return c;
   }
  }
  
  /*
  public static int getValueIndecimal(int n, int b){
      // Prescribed apporoach...
      int rv = 0;
      int p = 1;
      while(n!=0)
      {
          int r = (n % 10);
          n /= 10;
          
		  rv += r * p;
		  p *= b;
      }
      return rv;
   }
   */